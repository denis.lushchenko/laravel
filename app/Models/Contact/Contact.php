<?php

namespace App\Models\Contact;

use App\Models\Contact\CastsAttributes\ChoiceType;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'city',
        'country',
        'job_title',
        'type'
    ];

    protected $casts = [
        'type' => ChoiceType::class,
    ];
}
