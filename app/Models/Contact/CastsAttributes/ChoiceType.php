<?php

declare(strict_types = 1);

namespace App\Models\Contact\CastsAttributes;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Enum\ChoiceType as EnumChoiceType;

class ChoiceType implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): EnumChoiceType
    {
        return new EnumChoiceType(EnumChoiceType::MAIN);
    }

    /** @param $value EnumChoiceType */
    public function set($model, string $key, $value, array $attributes): string
    {
        return $value->getValue();
    }
}
