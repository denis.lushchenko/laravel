<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Http\ViewModels\ContactViewModel;
use App\Models\Contact\Contact;
use App\Services\ContactReportService;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    public function index(ContactReportService $contactReportService): Response
    {
        $reports = $contactReportService->getReport();

        return new Response(view('contacts.index', new ContactViewModel($reports)));
    }

    public function create(): Response
    {
        return new Response(view('contacts.create'));
    }

    public function store(ContactRequest $contactRequest, Contact $contact): Response
    {
        Contact::create($contactRequest->all());

        return redirect('/contacts')->with('success', 'Contact saved!');
    }

    public function edit(Contact $contact): Response
    {
        return new Response(view('contacts.edit', compact('contact')));
    }

    public function update(ContactRequest $contactRequest, Contact $contact): Response
    {
        $contact->update($contactRequest->all());

        return redirect('/contacts')->with('success', 'Contact updated!');
    }

    public function destroy(Contact $contact): Response
    {
        $contact->delete();

        return redirect('/contacts')->with('success', 'Contact deleted!');
    }
}
