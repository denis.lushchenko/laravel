<?php

declare(strict_types = 1);

namespace App\Http\ViewModels;

use App\Services\Dto\ContactReportDto;
use Spatie\ViewModels\ViewModel;

class ContactViewModel extends ViewModel
{
    /** @var ContactReportDto[] */
    private $reports;

    public function __construct(array $reports)
    {
        $this->reports = $reports;
    }

    public function reports(): array
    {
        return $this->reports;
    }
}
