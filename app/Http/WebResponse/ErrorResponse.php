<?php

declare(strict_types = 1);

namespace App\Http\WebResponse;

class ErrorResponse extends \Illuminate\Http\Response
{
    public function __construct(\Exception $exception, $status = 500, array $headers = [])
    {
        parent::__construct($exception->getMessage(), $status, $headers);
    }
}
