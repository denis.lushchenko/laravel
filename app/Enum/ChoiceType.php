<?php

declare(strict_types = 1);

namespace App\Enum;

class ChoiceType extends AbstractEnum
{
    public const MAIN   = 'main';
    public const SECOND = 'second';
    public const LAST   = 'last';
}
