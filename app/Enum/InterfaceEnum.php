<?php

declare(strict_types = 1);

namespace App\Enum;

interface InterfaceEnum
{
    /**
     * @param object|int|string $value
     */
    public function equals($value): bool;

    /**
     * @return int|string
     */
    public function getValue();
}
