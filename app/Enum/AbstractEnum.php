<?php

declare(strict_types = 1);

namespace App\Enum;

abstract class AbstractEnum implements InterfaceEnum
{
    /** @var int|string */
    private $value;

    /**
     * @param int|string $value
     */
    public function __construct($value)
    {
        $this->validate($value);

        $this->value = $value;
    }

    /**
     * @return int|string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param object|int|string $value
     */
    public function equals($value): bool
    {
        if (is_int($value) || is_string($value)) {
            return $this->getValue() === $value;
        } elseif (is_object($value) && $value instanceof InterfaceEnum) {
            return $this->getValue() === $value->getValue();
        } else {
            throw new \LogicException(
                sprintf(
                    'Equality allowed only for objects, integers and strings, "%s" given',
                    gettype($value)
                )
            );
        }
    }

    /**
     * @param int|string $value
     */
    protected function validate($value): void
    {
        if (!is_string($value) && !is_int($value)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Value must be of string or int type, "%s" given',
                    gettype($value)
                )
            );
        }
    }

    public static function getValueList(): array
    {
        return (new \ReflectionClass(static::class))->getConstants();
    }
}
