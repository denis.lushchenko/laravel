<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Models\Contact\Contact;
use Illuminate\Database\Eloquent\Collection;

class ContactRepository
{
    /** @return Contact[] */
    public function getAllContacts(): Collection
    {
        return Contact::all();
    }
}
