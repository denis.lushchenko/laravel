<?php

declare(strict_types = 1);

namespace App\Services;

use App\Repository\ContactRepository;
use App\Services\Dto\ContactReportDto;

class ContactReportService
{
    /** @var ContactRepository */
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function getReport(): array
    {
        $result = [];

        $contacts = $this->contactRepository->getAllContacts();

        foreach ($contacts as $contact) {
            $result[] = (new ContactReportDto())
                ->setContactId($contact->id)
                ->setReportFirstName($contact->first_name)
                ->setReportLastName($contact->last_name)
                ->setReportEmail($contact->email)
                ->setReportCity($contact->city)
                ->setReportCountry($contact->country)
                ->setReportJobTitle($contact->job_title)
                ->setReportType($contact->type)
                ->setSomeReportField('test');
        }

        return $result;
    }
}
