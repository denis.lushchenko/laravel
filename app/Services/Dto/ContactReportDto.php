<?php

declare(strict_types = 1);

namespace App\Services\Dto;

use App\Enum\ChoiceType;

class ContactReportDto
{
    /** @var int */
    private $contactId;

    /** @var string */
    private $reportFirstName;

    /** @var string */
    private $reportLastName;

    /** @var string */
    private $reportEmail;

    /** @var string */
    private $reportCity;

    /** @var string */
    private $reportCountry;

    /** @var string */
    private $reportJobTitle;

    /** @var ChoiceType */
    private $reportType;

    /** @var string */
    private $someReportField;

    public function getContactId(): int
    {
        return $this->contactId;
    }

    public function setContactId(int $contactId): self
    {
        $this->contactId = $contactId;

        return $this;
    }

    public function getReportFirstName(): string
    {
        return $this->reportFirstName;
    }

    public function setReportFirstName(string $reportFirstName): self
    {
        $this->reportFirstName = $reportFirstName;

        return $this;
    }

    public function getReportLastName(): string
    {
        return $this->reportLastName;
    }

    public function setReportLastName(string $reportLastName): self
    {
        $this->reportLastName = $reportLastName;

        return $this;
    }

    public function getReportEmail(): string
    {
        return $this->reportEmail;
    }

    public function setReportEmail(string $reportEmail): self
    {
        $this->reportEmail = $reportEmail;

        return $this;
    }

    public function getReportCity(): string
    {
        return $this->reportCity;
    }

    public function setReportCity(string $reportCity): self
    {
        $this->reportCity = $reportCity;

        return $this;
    }

    public function getReportCountry(): string
    {
        return $this->reportCountry;
    }

    public function setReportCountry(string $reportCountry): self
    {
        $this->reportCountry = $reportCountry;

        return $this;
    }

    public function getReportJobTitle(): string
    {
        return $this->reportJobTitle;
    }

    public function setReportJobTitle(string $reportJobTitle): self
    {
        $this->reportJobTitle = $reportJobTitle;

        return $this;
    }

    public function getReportType(): ChoiceType
    {
        return $this->reportType;
    }

    public function setReportType(ChoiceType $reportType): self
    {
        $this->reportType = $reportType;

        return $this;
    }

    public function getSomeReportField(): string
    {
        return $this->someReportField;
    }

    public function setSomeReportField(string $someReportField): self
    {
        $this->someReportField = $someReportField;

        return $this;
    }
}
