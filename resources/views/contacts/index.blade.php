@extends('base')

@section('main')
    <div class="col-sm-12">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
    </div>

    <div>
        <a style="margin: 19px;" href="{{ route('contacts.create')}}" class="btn btn-primary">New contact</a>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Contacts</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>Report ID</td>
                    <td>Report Name</td>
                    <td>Report Email</td>
                    <td>Report Job Title</td>
                    <td>Report City</td>
                    <td>Report Country</td>
                    <td colspan=2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $report)
                    <tr>
                        <td>{{$report->getReportFirstName()}} {{$report->getReportLastName()}}</td>
                        <td>{{$report->getReportEmail()}}</td>
                        <td>{{$report->getReportJobTitle()}}</td>
                        <td>{{$report->getReportCity()}}</td>
                        <td>{{$report->getReportCountry()}}</td>
                        <td>{{$report->getReportType()->getValue()}}</td>
                        <td>
                            <a href="{{ route('contacts.edit',$report->getContactId())}}"
                               class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('contacts.destroy', $report->getContactId())}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
